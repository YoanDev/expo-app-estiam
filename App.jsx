import { StatusBar } from "expo-status-bar";
import { StyleSheet, LogBox, View, ScrollView } from "react-native";
import BookSection from "./views/BookSection";
import CategorySection from "./views/CategorySection";
import HeaderSection from "./views/HeaderSection";
import HomeScreen from "./views/HomeScreen";
import { COLORS } from "./constants/theme";

// Hide Error FlatList using inside ScrollView
LogBox.ignoreLogs(["VirtualizedLists"]);

export default function App() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <HomeScreen />
        <HeaderSection />
        <BookSection />
        <CategorySection />
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.black,
    color: "#fff",
  },
});
