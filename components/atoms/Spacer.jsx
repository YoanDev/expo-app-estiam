import React from "react";
import { View } from "react-native";

const Spacer = ({ width, height }) => {
  return (
    <View
      style={{
        width,
        height,
      }}
    ></View>
  );
};

export default Spacer;
