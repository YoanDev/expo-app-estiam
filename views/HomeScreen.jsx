import React, { useState } from "react";
import { StyleSheet, View, Text, Pressable } from "react-native";
import { COLORS } from "../constants/theme";

const HomeScreen = () => {
  const [homeTitle, setHomeTitle] = useState("Home");

  const changeHomeTitle = (newTitle) => {
    setHomeTitle(newTitle);
  };
  return (
    <View style={styles.homeScreen}>
      <Pressable onPress={() => changeHomeTitle("Book Store App")}>
        <Text style={styles.text}>{homeTitle}</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLORS.white,
    fontSize: 18,
  },
  homeScreen: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: COLORS.primary,
    padding: 10,
    height: 80,
  },
});

export default HomeScreen;
