import React, { useState } from "react";
import { StyleSheet, View, Text, Pressable, Image } from "react-native";
import { user } from "../data/user";
import { COLORS } from "../constants/theme";
import Spacer from "../components/atoms/Spacer";

const HeaderSection = () => {
  const [userData, setUserData] = useState(user);

  const onPressFunction = (username) => {
    console.log(username);
  };
  return (
    <View style={styles.headerSection}>
      <View style={styles.block}>
        <Text style={styles.text}>Good morning</Text>
        <Text style={styles.textUsername}>{userData.name}</Text>
      </View>
      <View style={styles.block}>
        <Pressable onPress={() => onPressFunction(userData.name)}>
          <View style={styles.container}>
            <Image
              style={styles.icon}
              source={require("../assets/icons/plus_icon.png")}
            />
            <Spacer width={10} />
            <Text style={styles.text}>{userData.point} points</Text>
          </View>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLORS.white,
  },
  textUsername: {
    color: COLORS.white,
    fontSize: 22,
  },
  headerSection: {
    height: 200,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: COLORS.primary,
    padding: 10,
    borderRadius: 20,
  },
  icon: {
    width: 15,
    height: 15,
    backgroundColor: COLORS.black,
    borderRadius: 50,
    padding: 10,
    opacity: 0.7,
  },
  block: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    flex: 1,
  },
});

export default HeaderSection;
