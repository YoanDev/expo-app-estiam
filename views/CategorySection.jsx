import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  FlatList,
} from "react-native";
import { COLORS } from "../constants";
import { categories } from "../data";

const CategorySection = () => {
  const [categoryData, setCategoryData] = useState(categories);
  const [titleActive, setTitleActive] = useState(categories[0].categoryName);

  const changeTitleActive = (title) => {
    setTitleActive(title);
  };
  return (
    <View style={styles.containerCol}>
      <ScrollView horizontal>
        <View style={styles.barCategory}>
          {categoryData.map((category) => {
            return (
              <Pressable
                onPress={() => changeTitleActive(category.categoryName)}
              >
                <Text
                  style={
                    titleActive === category.categoryName
                      ? styles.titleActive
                      : styles.title
                  }
                >
                  {category.categoryName}
                </Text>
              </Pressable>
            );
          })}
        </View>
      </ScrollView>
      <View style={styles.containerCol}>
        {categories.map((category) => {
          return (
            <ScrollView>
              {titleActive === category.categoryName &&
                category.books.map((book) => {
                  return (
                    <Pressable>
                      <View style={styles.containerRow}>
                        <Image
                          source={book.bookCover}
                          style={styles.imageBook}
                        />
                        <View style={styles.columnBox}>
                          <Text style={styles.bookTitle}>{book.bookName}</Text>
                          <Text style={styles.bookAuthor}>{book.author}</Text>
                          <View style={styles.row}>
                            <View style={styles.row}>
                              <Image
                                source={require("../assets/icons/page_filled_icon.png")}
                                style={styles.icon}
                              />
                              <Text style={styles.text}>{book.pageNo}</Text>
                            </View>
                            <View style={styles.row}>
                              <Image
                                source={require("../assets/icons/read_icon.png")}
                                style={styles.icon}
                              />
                              <Text style={styles.text}>{book.readed}</Text>
                            </View>
                          </View>
                          <View style={styles.containerGenre}>
                            {book.genre.map((genre) => {
                              return <Text style={styles.genre}>{genre}</Text>;
                            })}
                          </View>
                        </View>
                      </View>
                    </Pressable>
                  );
                })}
            </ScrollView>
          );
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLORS.lightGray,
    padding: 10,
  },
  bookTitle: {
    color: COLORS.white,
    padding: 10,
    fontSize: 18,
  },
  bookAuthor: {
    color: COLORS.lightGray,
    padding: 10,
    fontSize: 14,
  },
  title: {
    color: COLORS.lightGray5,
    padding: 10,
    fontSize: 20,
  },
  titleActive: {
    color: COLORS.white,
    padding: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  barCategory: {
    flexDirection: "row",
  },
  containerRow: {
    flexDirection: "row",
    flex: 1,
    width: "100%",
    backgroundColor: COLORS.darkBlue,
    padding: 20,
    margin: 20,
  },
  containerCol: {
    flexDirection: "column",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  imageBook: {
    width: 150,
    height: 200,
    borderRadius: 8,
  },
  columnBox: {
    flexDirection: "column",
  },
  icon: {
    width: 15,
    height: 15,
    tintColor: COLORS.lightGray,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  genre: {
    color: COLORS.lightGreen,
    margin: 5,
    padding: 5,
    backgroundColor: COLORS.black,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    textAlign: "center",
  },

  containerGenre: {
    padding: 10,
    flexWrap: "wrap",
    flexDirection: "row",
    width: 200,
  },
});

export default CategorySection;
