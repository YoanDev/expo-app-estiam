import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
} from "react-native";
import { COLORS } from "../constants";
import { myBooks } from "../data";

const BookSection = () => {
  const [booksData, setBooksData] = useState(myBooks);
  return (
    <>
      <ScrollView horizontal>
        <View style={styles.bookSection}>
          <View style={styles.container}>
            <Text style={styles.textStyle}>My Book</Text>
            <Text style={styles.textStyle}>See More</Text>
          </View>
          <ScrollView>
            <View style={styles.containerBook}>
              {booksData.map((book) => {
                return (
                  <Pressable>
                    <View style={styles.bookCard}>
                      <Image style={styles.bookCover} source={book.bookCover} />
                      <View style={styles.containerBox}>
                        <View style={styles.wrapper}>
                          <Image
                            source={require("../assets/icons/clock_icon.png")}
                            style={styles.icon}
                          />
                          <Text style={styles.text}>{book.lastRead}</Text>
                        </View>
                        <View style={styles.wrapper}>
                          <Image
                            source={require("../assets/icons/page_icon.png")}
                            style={styles.icon}
                          />
                          <Text style={styles.text}>{book.completion}</Text>
                        </View>
                      </View>
                    </View>
                  </Pressable>
                );
              })}
            </View>
          </ScrollView>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    color: COLORS.lightGray5,
    padding: 10,
  },
  textStyle: {
    color: COLORS.white,
    padding: 10,
    fontSize: 22,
  },
  bookSection: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: 400,
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexDirection: "row",
  },
  containerBook: {
    flexDirection: "row",
    flex: 1,
  },
  containerBox: {
    flexDirection: "row",
    flex: 1,
  },
  bookCard: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    padding: 20,
  },
  bookCover: {
    width: 160,
    height: 160,
  },
  icon: {
    width: 15,
    height: 15,
    tintColor: COLORS.lightGray5,
  },
  wrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
});

export default BookSection;
